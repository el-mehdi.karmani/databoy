## List of the components

<table>
    <thead>
        <tr>
            <th>Component </th>
            <th>Indices on Figure 5</th>
            <th>Reference </th>
            <th>Characteristic  </th>
            <th>Price Unit </th>
            <th>Quantity </th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td rowspan=2>Zener Diode </td>
            <td>D1 </td>
            <td>Vishay BZX55C12-TAP </td>
            <td>Zener voltage: 12V </td> 
            <td>0.089e </td> 
            <td>1 </td>
        </tr>
        <tr>
            <td>D2 D3 </td>
            <td>Vishay TZX5V1B-TR </td>
            <td>Zener voltage: 5 V </td> 
            <td>0.073e </td> 
            <td>3 </td>
        </tr>
        <tr>
            <td rowspan=5>Resistor </td>
            <td>R1, R2, R3, R4, R5, R6</td>
            <td>Arcol MRA02071KBTA015 </td>
            <td>Resistance: 1 kΩ,</br>
                Rated power: 0.25 W,</br>
                Tolerance: 0.1 % </td> 
            <td>1.272e </td> 
            <td>6 </td>
        </tr>
        <tr>
            <td>R7, R11</td>
            <td>TE Connectivity YR1B200KCC </td>
            <td>Resistance: 200 kΩ,</br>
                Rated power: 0.25 W,</br>
                Tolerance: 0.1 % </td> 
            <td>0.44e </td> 
            <td>2 </td>
        </tr>
        <tr>
            <td> R8, R9, R12, R13, R15 </td>
            <td>TE Connectivity YR1B20RCC </td>
            <td>Resistance: 20 kΩ,</br>
                Rated power: 0.25 W,</br>
                Tolerance: 0.1 % </td> 
            <td>0.594e </td> 
            <td>5 </td>
        </tr>
        <tr>
            <td>R10, R14</td>
            <td>TE Connectivity YR1B475RCC </td>
            <td>Resistance: 475 Ω,</br>
                Rated power: 0.25 W,</br>
                Tolerance: 0.1 % </td> 
            <td>0.456e </td> 
            <td>2 </td>
        </tr>
        <tr>
            <td>R16</td>
            <td> Vishay MRS25000C1004FCT00 </td>
            <td>Resistance: 1 MΩ,</br>
                Rated power: 0.25 W,</br>
                Tolerance: 0.1 % </td> 
            <td>0.178e </td> 
            <td>1 </td>
        </tr>
        <tr>
            <td rowspan=3>Capicitor </td>
            <td>C1, C4</td>
            <td>KEMET R82DC3470DQ60J </td>
            <td>Capicitance: 470 nF,</br>
                Tolerance: ±5 %,</br>
                Voltage: 40 V(AC), 63 V(DC) </td> 
            <td>0.15e </td> 
            <td>2 </td>
        </tr>
        <tr>
            <td>C2, C5</td>
            <td>KEMET R82EC2220DQ50J </td>
            <td>Capicitance: 22 nF,</br>
                Tolerance: ±5 %,</br>
                Voltage: 40 V(AC), 63 V(DC) </td> 
            <td>0.162e </td> 
            <td>2 </td>
        </tr>
        <tr>
            <td>C1, C4</td>
            <td>KEMET MMK15225J63B05L4BULK </td>
            <td>Capicitance: 2.2 µF,</br>
                Tolerance: ±5 %,</br>
                Voltage: 40 V(AC), 63 V(DC) </td> 
            <td>1.204e </td> 
            <td>2 </td>
        </tr>
        <tr>
            <td>Frequency to voltage converter </td>
            <td>LM2907</td>
            <td>Texas Instrument LM2907N-8/NOPB </td>
            <td>Supply voltage: 0 V to 28 V </br>
                Supply current: 25 mA </br>
                Input voltage: 0 V to the supply voltage</br>
                Maximum Frequency (in that case): 606 Hz </td> 
            <td>2.46e </td> 
            <td>2 </td>
        </tr>
        <tr>
            <td>Isolation amplifier </td>
            <td>AD202JY</td>
            <td>ANALOG DEVICES AD202JY </td>
            <td>Supply voltage: 15V </br>
                Input voltage: ±5 voltage</br>
                Output voltage: +5 voltage</br> </td> 
            <td>90.60e </td> 
            <td>1 </td>
        </tr>
        <tr>
            <td>Current transducer </td>
            <td>HX-50-P/SP2</td>
            <td>LEM HX-50-P/SP2 </td>
            <td>Supply voltage: 15V </br>
                Input current: 0A to 50A(AC)</br>
                0A to 50 A(AC)</td> 
            <td>17.37e </td> 
            <td>1 </td>
        </tr>
        <tr>
            <td>DC/DC converter</td>
            <td>DC/DC converter</td>
            <td>POLOLU U3V50AHV </td>
            <td>Input voltage: 2.9 V to Vout</br> 
               V_out range (adjustable): 9 V to 30 V</br>
               Efficiency (V_in=12 V, V_out=18 V): ~95 % </td> 
            <td>21.69e </td> 
            <td>1 </td>
        </tr>
        <tr>
            <td>Terminal block 1</td>
            <td> </td> 
            <td>TE Connectivity 282837-2</td>
            <td>2 contacts,</br> 1 range, 5.08 mm </td>
            <td>0.387e </td> 
            <td>12 </td>
        </tr>
        <tr>
            <td>Terminal block 2</td>
            <td> </td> 
            <td>RS PRO 790-1073</td>
            <td>3 contacts,</br> 1 range, 5.08 mm </td>
            <td>0.387e </td> 
            <td>1 </td>
        </tr>
        <tr>
            <td>Terminal block 3</td>
            <td> </td> 
            <td>RS PRO 790-1098</td>
            <td>2 contacts,</br> 1 range, 2.54 mm </td>
            <td>0.387e </td> 
            <td>2 </td>
        </tr>
        <tr>
            <td>Terminal block 1</td>
            <td> </td> 
            <td>Phoenix contact 1725711</td>
            <td>8 contacts,</br> 1 range, 2.54 mm </td>
            <td>5.544e </td> 
            <td>2 </td>
        </tr>
        <tr>
            <td>Solar charge controller</td>
            <td> Solar charge controller</td> 
            <td>Victron BlueSolar MPPT 75–10</td>
            <td>PV voltage range: 0 V to 100 V</br>
                Nominal battery voltage: 12 V</br>
                Charging current range: 0 A to 10 A </td>
            <td>82.62e </td> 
            <td>1 </td>
        </tr>
        <tr>
            <td>RaspberryPi</td>
            <td> </td> 
            <td>RaspberryPi3</td>
            <td>Voltage supply:5 V </td>
            <td>59.84e </td> 
            <td>1 </td>
        </tr>
        <tr>
            <td>RaspberryPi converter</td>
            <td> </td> 
            <td>JOY IT STROM PI</td>
            <td>Input voltage: 6 V to 61 V</br>
                Output voltage: 5 V </td>
            <td>33.99e </td> 
            <td>1 </td>
        </tr>
        <tr>
            <td>ADC</td>
            <td> </td> 
            <td>ABElectronics ADCPi</td>
            <td>Voltage supply: 5 V </br>
                inputs: 8 ADC connectors, 25 GPIO pins</br>
                Input voltage: 0 V to 5 V</br>
                Output voltage: 5 V </td>
            <td>18.67e </td> 
            <td>2 </td>
        </tr>
        <tr>
            <td>RTC</td>
            <td> </td> 
            <td>ABElectronics, RTC Pi</td>
            <td>Voltage supply: 5 V </td>
            <td>9.63e </td> 
            <td>1 </td>
        </tr>
        <tr>
            <td>Peripheric</td>
        </tr>
        <tr>
            <td>Battery</td>
            <td> </td> 
            <td>YUASA Y12–12L</td>
            <td>12 V, 12 Ah, lead-acid </td>
            <td>43.60e </td> 
            <td>1 </td>
        </tr>
        <tr>
            <td>Thermocouple</td>
            <td>RPT1, RPT2 </td> 
            <td> RS Pro PT1000</td>
            <td>Temperature range: 0 °C to +500 °C </br>
                Precision: ±0.05 %  </td>
            <td>8.08e </td> 
            <td>2 </td>
        </tr>
        <tr>
            <td>irradiance sensor</td>
            <td>Irr1 </td> 
            <td>SOLEMS RG100</td>
            <td>1000 W/m2 = 0.1 V </td>
            <td> </td> 
            <td>1 </td>
        </tr>
        <tr>
            <td>INSIDE USB stick</td>
            <td> </td> 
            <td>Verbatim 98130</td>
            <td>32 Go</td>
            <td>7.98e </td> 
            <td>2 </td>
        </tr>
        <tr>
            <td>OUTSIDE USB stick</td>
            <td> </td> 
            <td>Transcend TS8GJF600</td>
            <td>8 Go</td>
            <td>11.34e </td> 
            <td>1 </td>
        </tr>
        <tr>
            <td>Circuit breaker</td>
            <td> </td> 
            <td>Europa EUC1P10C</td>
            <td>Break power: 10 kA </br>
                Nominal current: 10 A </td>
            <td>7.33e </td> 
            <td>3 </td>
        </tr>
        <tr>
            <td>Datalogger box</td>
        </tr>
        <tr>
            <td>The box</td>
            <td> </td> 
            <td>FIBOX EKUH 180 G </td> 
            <td>Size: 560x380x180 mm </br>
                Material: Polycarbonate</br>
                IP 67 </td>
            <td>180e </td> 
            <td>1 </td>
        </tr>
        <tr>
            <td>Mounting plate</td>
            <td> </td> 
            <td>FIBOX EKUVT </td> 
            <td>Size: 518x1.5x338 mm </br>
                Material: Metal</br>
                IP 67 </td>
            <td>28.79e </td> 
            <td>1 </td>
        </tr>
        <tr>
            <td>USB connector panel mount</td>
            <td> </td> 
            <td>ENCITECH 1310-1018-01 </td> 
            <td>USB 2.0 type A </br>
                IP 67 </td>
            <td>Break power: 10 kA </br>
                Nominal current: 10 A </td>
            <td>35.83e </td> 
        </tr>
        <tr>
            <td>RED Led</td>
            <td> </td> 
            <td>KingBright L-53HD </td> 
            <td>Voltage supply: 2.25 V</td>
            <td>0.318e </td> 
            <td>1 </td>
        </tr>
        <tr>
            <td>Yellow Led</td>
            <td> </td> 
            <td>KingBright L-53YD </td> 
            <td>Voltage supply: 2.1 V</td>
            <td>0.212e </td> 
            <td>1 </td>
        </tr>
        <tr>
            <td>BNC connectors</td>
            <td> </td> 
            <td>Radiall R141605000W</td>
            <td>Impedance: 50 Ω </br>
                IP 67 </td> 
            <td>9.03e </td> 
            <td>7 </td>
        </tr>
        <tr>
            <td>“Rondelle de masse”</td>
            <td> </td> 
            <td>RS PRO 275-3011</td> 
            <td>Diameter: 9.7 mm</td>
            <td>0.416e </td> 
            <td>7 </td>
        </tr>
        <tr>
            <td>MC4 connectors</td>
            <td> </td> 
            <td>STAUBLI 32.0078P0001+32.0079P0001</td> 
            <td>Maximum current: 30 A</br>
                IP 67 </td>
            <td>6.65e </td> 
            <td>8 </td>
        </tr>
    </tbody>
</table>
<br/>
Table – Components used in the Datalogger V3.1. 

Hardware cost = 884,27€.
