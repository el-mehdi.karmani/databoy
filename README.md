![datalogger_logo](https://gitlab-student.centralesupelec.fr/el-mehdi.karmani/databoy/uploads/4b266e9dfd5ab9f5eea5a0284baab9f2/datalogger_logo.png)

# Overview <br/> 
DataBoy is an Open Source Hardware Datalogger designed to monitor photovoltaic water pumping systems in remote areas. For example, the Datalogger V1.0 has been monitoring an installation since January 2018 in the village of Gogma in Burkina Faso since these villages are off-grid, the Datalogger V1.0 is supplied by batteries and photovoltaic (PV) panels. 

# Getting started <br/>

- Prerequisite: <br/>
   - KiCAD <br/> 
   - Git <br/> 
- Get started by cloning the repo `.git clone https://gitlab-student.centralesupelec.fr/el-mehdi.karmani/databoy.git`
- Launch KiCAD_files/datalogger.pro with KiCAD 
- Enjoy

# Design explanation  

## Requirements 
Project requirements are below: 

### Power supply 

Powered from a 12 V, 12 Ah, lead-acid external battery [YUASA Y12–12L](https://fr.rs-online.com/web/p/batteries-au-plomb/6142475).

### Data Display 

Two LEDs give information about the data collection and the connection of the USB stick. 

<table>
    <thead>
        <tr>
            <th>Yellow LED (ON/Blinking/Off) </th>
            <th>Red LED (ON/Blinking/Off) </th>
            <th>Information</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>OFF</td>
            <td>OFF</td>
            <td>No USB stick connected</td>
        </tr>
        <tr>
            <td>ON</td>
            <td>ON</td>
            <td>A USB stick is connected and data are being collected </td>
        </tr>
        <tr>
            <td> OFF </td>
            <td> ON</td>
            <td>A USB stick is connected but no data are being collected</td>
        </tr>
        <tr>
            <td> OFF </td>
            <td> Blinking</td>
            <td>A USB stick is connected but data have already been collected</td>
        </tr> 
    </tbody>
</table>
<br/>

### Data logging 
Local storage on SD card.

### PCB 

* Dimension 33x25 cm

* Only widely available component references

Note 1: Using only widely available components is important to ensure that we do not suffer from chip shortage and that people across the world can source components locally to repair the devices.

### Software design

KiCAD + export as gerber file

## IC description

Integrated circuits description can be found [here](https://gitlab-student.centralesupelec.fr/el-mehdi.karmani/databoy/-/blob/main/Datasheet/IC%20description.md).

## Measurement Blocks 

### Sensor description
Sensor description can be found [here](https://gitlab-student.centralesupelec.fr/el-mehdi.karmani/databoy/-/blob/main/Datasheet/Sensor%20description.md). <br/>

### Sensors

<table>
    <thead>
        <tr>
            <th>Sensor </th>
            <th>Name </th>
            <th>Signal </th>
            <th>Connector </th>
            <th>Description </th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Water flow sensor1 </td>
            <td>VFLOW1</td>
            <td>Square voltage <br/> (0 V low, 5 V high)</td>
            <td>BNC</td>
            <td>Square signal from the water flow sensor 1</td>
        </tr>
        <tr>
            <td>Water flow sensor2 </td>
            <td>VFLOW2</td>
            <td>Square voltage <br/> (0 V low, 5 V high)</td>
            <td>BNC</td>
            <td>Square signal from the water flow sensor 2</td>
        </tr>
        <tr>
            <td>Current sensor </td>
            <td>I1in</td>
            <td>Continuous voltage</td>
            <td>MC4</td>
            <td>Input current 1</td>
        </tr>
        <tr>
            <td>Voltage sensor </td>
            <td>V1in High, V1in Low</td>
            <td>Continuous voltage</td>
            <td>MC4</td>
            <td>Input voltage 1</td>
        </tr>
        <tr>
            <td>Temperature sensor 1</td>
            <td>RPT1</td>
            <td> </td>
            <td>BNC</td>
            <td>Variable resistance which increases with the temperature</td>
        </tr>
        <tr>
            <td>Temperature sensor 2</td>
            <td>RPT2</td>
            <td></td>
            <td>BNC</td>
            <td>Variable resistance which increases with the temperature</td>
        </tr>
        <tr>
            <td>Irradiance sensor</td>
            <td>Irr1</td>
            <td>Continuous Voltage</td>
            <td>BNC</td>
            <td>Proportional voltage of the irradiance from the irradiance sensor</td>
        </tr>
    </tbody>
</table>
<br/>

### Signal acquisition
Signal acquisition can be found [here](https://gitlab-student.centralesupelec.fr/el-mehdi.karmani/databoy/-/wikis/Signal-acquisition). 

### Signal conditioning 
Signal conditioning can be found [here](https://gitlab-student.centralesupelec.fr/el-mehdi.karmani/databoy/-/wikis/Signal-conditioning)
# Manuals 
### User Manual 
User manual can be found [here](https://gitlab-student.centralesupelec.fr/el-mehdi.karmani/databoy/-/wikis/User-manual)
### Calibration Manual 
Calibration Manual can be found [here](https://gitlab-student.centralesupelec.fr/el-mehdi.karmani/databoy/-/wikis/Calibration-manual)

# Licence 
This work is licensed under ********. Read carefully the license file attached

# Project status 

# Credit 
<table>
    <thead>
        <tr>
            <th>Review </th>
            <th>Date </th>
            <th>Authors </th>
            <th>Validation </th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>V1.0</td>
            <td>2017</td>
            <td>Simon Meunier</td>
            <td>Loïc Quéval</td>
        </tr>
        <tr>
            <td>V2.0</td>
            <td>2018</td>
            <td>Joséphine Demay<br/>Paul Lorang</td>
            <td>Loïc Quéval<br/>Simon Meunier</td>
        </tr>
        <tr>
            <td>V3.0</td>
            <td>2019</td>
            <td>Vincent Roy</td>
            <td>Loïc Quéval<br/>Simon Meunier</td>
        </tr>
        <tr>
            <td>V3.1</td>
            <td>2021</td>
            <td>Sébastien Ferreira</td>
            <td>Loïc Quéval<br/>Simon Meunier</td>
        </tr>
        <tr>
            <td>V3.2</td>
            <td>2022</td>
            <td>El Mehdi Karmani<br/>Ayoub Harchadi<br/>Lokmane Haouchine</td>
            <td>Loïc Quéval<br/>Simon Meunier</td>
        </tr>
    </tbody>
</table>
<br/>
