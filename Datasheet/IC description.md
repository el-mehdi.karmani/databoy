# Integrated circuits description

[AD202-1501577](https://www.analog.com/media/en/technical-documentation/data-sheets/ad202_204.pdf) : Two-port, transformer-coupled isolation amplifiers for applications where input signals must be measured, processed, and/or transmitted without a galvanic connection.

[ADC_pi_plus](https://www.abelectronics.co.uk/p/69/adc-pi-raspberry-pi-analogue-to-digital-converter) : 8 channel 17 bit analog to digital converter works with the Raspberry Pi.

[hx_03_50-p_ver15](https://www.lem.com/sites/default/files/products_datasheets/hx_03_50-p_ver15.pdf) : Current Transducer for the electronic measurement of currents with galvanic separation between the primary and the secondary circuit.

[LM741](https://www.ti.com/lit/ds/symlink/lm741.pdf) : Operational Amplifier provides overload protection on the input and output, no latch-up when the common mode range is exceeded, as well as freedom from oscillations.

[LM2907](https://www.ti.com/lit/gpn/lm2907-n) : Monolithic frequency-to-voltage converters with a high gain op amp designed to operate a relay, lamp, or other load when the input frequency reaches or exceeds a selected rate.

[LT1013](https://pdf1.alldatasheet.com/datasheet-pdf/view/1102436/MORNSUN/B0303S-1WR2.html) isolated DC/DC converter (3V3 to 3V3)

[LT1014](https://www.analog.com/media/en/technical-documentation/data-sheets/lt1013-lt1014.pdf) : Dual precision operational amplifiers, featuring high gain, low supply current, low noise, and low-offset-voltage temperature coefficient.

[MCP601IP](https://ww1.microchip.com/downloads/en/DeviceDoc/21314g.pdf) :Single Supply CMOS Op Amps provides low bias current, highspeed operation, high open-loop gain, and rail-to-rail output swing.

[POLOLU-2571](https://www.pololu.com/product/2571) : Adjustable boost (step-up) voltage regulators generate higher output voltages from input voltages.

[StromPi2](https://joy-it.net/en/products/RB-StromPi2) Allows the Raspberry Pi to use any voltage source with a voltage range of 6-61 V, to connect larger/multiple batteries or voltage supplies.

