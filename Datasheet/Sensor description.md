# Sensors description

[PT100](https://www.pyrosales.com.au/blog/rtds/pt100-temperature-sensor-datasheet/) : Platinum resistance thermometer.

[RG100](https://www.solems.com/wp-content/uploads/Solar_radiation_sensors_RG.pdf) : Irradiation sensor, an output of 100 mVDC is delivered for an incidence of 1000W/m² STC – Standard Test Conditions- It is therefore compatible with any voltmeter or datalogger.

[FS300A G3/4](https://www.hotmcu.com/g34-water-flow-sensor-p-311.html) :water flow sensor consists of magnetic core, rotating impeller, external casing and sensor and a hall-effect sensor. When water flows through the rotor, rotor rolls, it activates the magnetic core to trigger switch action speed changes with different rate of flow.

